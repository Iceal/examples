package examples.cusip;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class CusipSampleGenerator {

    private static final Random random = new Random();

    public static final List<String> SAMPLE_SYMBOLS = Arrays.asList("AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF", "GGGG", "HHHH", "IIII", "JJJJ");

    public static final String CUSIP_FILE_PATH = System.getProperty("user.home") + File.separator + "cusip.log";

    private static final Map<String, BigDecimal> SAMPLE_VALUES = new HashMap<>();

    private void init() throws IOException {
        File file = new File(CUSIP_FILE_PATH);
        if(!file.exists()) {
            file.createNewFile();
        }

        SAMPLE_SYMBOLS.forEach( symbol -> {
            SAMPLE_VALUES.put(symbol, getRandomValue());
        });
    }

    public void generateNSamples(int n) throws IOException {
        init();
        int periods = 0;
        while(periods < n) {
            generateSamples();
            periods++;
        }
    }

    private void generateSamples() {
        SAMPLE_SYMBOLS.forEach(symbol -> {
            try {
                String symbolToWrite = symbol + "\n";
                Files.write(Paths.get(System.getProperty("user.home") + File.separator + "cusip.log"), symbolToWrite.getBytes(), StandardOpenOption.APPEND);
                for(int i=0; i<random.nextInt(10) + 1; i++) {
                    BigDecimal currentValue = SAMPLE_VALUES.get(symbol);
                    String valueToWrite = currentValue.toString() + "\n";
                    Files.write(Paths.get(System.getProperty("user.home") + File.separator + "cusip.log"), valueToWrite.getBytes(), StandardOpenOption.APPEND);
                    SAMPLE_VALUES.put(symbol, getNextRandom(currentValue));
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    private BigDecimal getRandomValue() {
        return new BigDecimal(random.nextInt(1000) * Math.random()).setScale(2, RoundingMode.CEILING);
    }

    private BigDecimal getNextRandom(BigDecimal currentValue) {
        BigDecimal delta = currentValue.multiply(new BigDecimal(0.01));
        BigDecimal nextValue = currentValue.subtract(delta).setScale(2, RoundingMode.CEILING);
        if(random.nextInt(2) == 0) {
            nextValue = currentValue.add(delta).setScale(2, RoundingMode.CEILING);
        }
        if( nextValue.compareTo(new BigDecimal(0.0000)) < 1 ) {
            return currentValue;
        } else {
            return nextValue;
        }
    }
}