package examples.cusip;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.stream.Stream;

public class CusipReader {

    public void displayLast(final String filePath, final List<String> expectedSymbols) throws IOException {
        final Map<String, BigDecimal> lastMap = new HashMap<>();
        final Stack<String> lastSymbol = new Stack<String>();
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(line -> {
                if (hasAlpha(line)) {
                    lastSymbol.empty();
                    lastSymbol.push(line);
                } else {
                    lastMap.put(lastSymbol.peek(), new BigDecimal(line));
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        expectedSymbols.forEach(symbol -> {
            System.out.println(String.format("%s\n\tLast: %s\n", symbol, lastMap.get(symbol).toString()));
        });
    }

    private boolean hasAlpha(String symbol) {
        for(int i=0; i<symbol.length(); i++) {
            if(Character.isAlphabetic(symbol.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) throws IOException {
        CusipSampleGenerator sampleGenerator = new CusipSampleGenerator();
        sampleGenerator.generateNSamples(100);
        CusipReader reader = new CusipReader();
        System.out.println("Market Closed:\n\n");
        reader.displayLast(CusipSampleGenerator.CUSIP_FILE_PATH, CusipSampleGenerator.SAMPLE_SYMBOLS);
    }
}