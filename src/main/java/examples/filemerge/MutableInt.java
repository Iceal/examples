package examples.filemerge;

public class MutableInt {

    private Integer value;

    private boolean init = false;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        init = true;
        this.value = value;
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }
}
