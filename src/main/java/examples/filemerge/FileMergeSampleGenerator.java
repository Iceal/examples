package examples.filemerge;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Random;

public class FileMergeSampleGenerator {

    public static final String FIRST_FILE_PATH = System.getProperty("user.home") + File.separator + "first.log";
    public static final String SECOND_FILE_PATH = System.getProperty("user.home") + File.separator + "second.log";

    private static final Random random = new Random();

    public void generateSamples(int n) throws IOException {
        for(String filename : Arrays.asList(FIRST_FILE_PATH, SECOND_FILE_PATH)) {
            File file = new File(filename);
            if(file.exists()) {
                file.delete();
            }
            file.createNewFile();
        }

        for(int i=0; i<n; i++) {
            String outputFile = SECOND_FILE_PATH;
            if(random.nextInt(2) == 0) {
                outputFile = FIRST_FILE_PATH;
            }
            String valueToWrite = String.valueOf(i) + "\n";
            Files.write(Paths.get(outputFile), valueToWrite.getBytes(), StandardOpenOption.APPEND);
        }
    }
}