package examples.filemerge;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;

public class SortedFileMerge {

    public static final String MERGED_FILE_PATH = System.getProperty("user.home") + File.separator + "merge.log";

    public String merge(String first, String second) throws IOException {

        File file = new File(MERGED_FILE_PATH);
        if(file.exists()) {
            file.delete();
        }
        file.createNewFile();

        Iterator<String> firstIter = Files.lines(Paths.get(first)).iterator();
        Iterator<String> secondIter = Files.lines(Paths.get(second)).iterator();

        Integer firstValue = null;
        Integer secondValue = null;

        while(firstIter.hasNext() || secondIter.hasNext()) {

            if(firstValue == null && firstIter.hasNext()) {
                firstValue = Integer.valueOf(firstIter.next());
            }

            if(secondValue == null && secondIter.hasNext()) {
                secondValue = Integer.valueOf(secondIter.next());
            }

            if(firstValue != null && secondValue != null && firstValue < secondValue) {
                Files.write(Paths.get(MERGED_FILE_PATH), toBytes(firstValue), StandardOpenOption.APPEND);
                firstValue = null;
            } else if (firstValue != null && secondValue != null) {
                Files.write(Paths.get(MERGED_FILE_PATH), toBytes(secondValue), StandardOpenOption.APPEND);
                secondValue = null;
            } else if (firstValue != null) {
                Files.write(Paths.get(MERGED_FILE_PATH), toBytes(firstValue), StandardOpenOption.APPEND);
                firstValue = null;
            } else if (secondValue != null) {
                Files.write(Paths.get(MERGED_FILE_PATH), toBytes(secondValue), StandardOpenOption.APPEND);
                secondValue = null;
            }
        }


        return MERGED_FILE_PATH;
    }

    public void verifySorted(String filepath) throws IOException {
        final MutableInt prev = new MutableInt();
        Files.lines(Paths.get(filepath)).forEach(line -> {
            Integer curValue = Integer.valueOf(line);
            if(!prev.isInit()) {
                prev.setValue(curValue);
            } else {
                if(prev.getValue() > curValue) {
                    throw new RuntimeException("File is not sorted!");
                }
                prev.setValue(curValue);
            }
        });
    }

    private byte[] toBytes(Integer value) {
        return (value + "\n").getBytes();
    }

    public static void main(String[] args) throws IOException {
        FileMergeSampleGenerator sampleGenerator = new FileMergeSampleGenerator();
        sampleGenerator.generateSamples(100);
        SortedFileMerge sortor = new SortedFileMerge();
        final String outputFile = sortor.merge(FileMergeSampleGenerator.FIRST_FILE_PATH, FileMergeSampleGenerator.SECOND_FILE_PATH);
        sortor.verifySorted(outputFile);
        System.out.println("OK!");
    }
}
